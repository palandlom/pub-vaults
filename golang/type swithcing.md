---
aliases: []
---
#assertion #interface #type #golang 

В примере особое [[type assertion]] `.(type)` пытается в каждом `case`-блоке привести переменную `a` к указанному в этом блоке типу. Если это получается, `case` срабатывает и внутри него переменная `a`  приобретает заданный тип и результат идет в `assertedVal`

```golang
	switch assertedVal := a.(type) {
	default:
		fmt.Printf("unexpected type %T\n", assertedVal)     // %T prints whatever type a has
	case bool:
		fmt.Printf("boolean %t\n", assertedVal)             
		// here assertedVal is  bool
	case int:
		fmt.Printf("integer %d\n", assertedVal)             
	case *bool:
		fmt.Printf("pointer to boolean %t\n", *assertedVal) // a has type *bool
	case *int:
		fmt.Printf("pointer to integer %d\n", *assertedVal) // a has type *int
	}
```

