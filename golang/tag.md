#tag #build

Тэги ставятся в самом верху файла + за ними следует пустая строка:
```golang
// go:build tag1,tag2 tag3,!tag4
// +build tag5

package main

func foo() {
	fmt.Printf("This is alternative foo\n")
}
```

В примере выше тэги образуют условие  `(tag1 AND tag2) OR (tag3 AND (NOT tag4)) AND tag5` где: 
* `AND` == `,` 
* `OR` == `пробел` 
* `NOT` == `!` 

Если при запуске команды с набором тэгом, тэг-условие файла выполняется, то файл используется:
```golang
go build -tags tag4 -o main_alt .
go build -tags "tag1 tag2" // tag1 or tag2 - не проверено
```

Note:
```golang
//go:build !nolicence   // new style + preferred
// +build !nolicence // old style
```