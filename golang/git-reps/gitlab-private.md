Ставим [[GOPROXY-переменные]]:
```bash
    export GOPRIVATE=my.gitlab.site/*
    export GOPROXY=direct
    export GONOSUMDB=my.gitlab.site/*
```

Пример - To use the GoCenter public GOPROXY along with private modules, set the Golang environment variables:
```bash
    $ export GOPROXY=https://gocenter.io,direct
    $ export GOPRIVATE=*.internal.mycompany.com
```

Получаем токен из приватноге гит-лаба:
    Sign in to GitLab.
    In the top-right corner, select your avatar.
    Select Edit profile.
    In the left sidebar, select Access Tokens.
    Choose a name and optional expiry date for the token.
    Choose the desired scopes.(Choose all scopes)
    Select Create personal access token.
    Save the personal access token somewhere safe. If you navigate away or refresh your page, and you did not save the token, you must create a new one.

Add token:

```bash
	git config   --global \
	  url."https://pavel_lomov:tokenSDFSF@my.gitlab.site".insteadOf \
	  "https://my.gitlab.site"
```

Now get module:
    go get my.gitlab.site/sekai-backend/logic-api@v0.9.9
    go get my.gitlab.site/sekai-backend/logic-api/v2

