* GOPROXY = redirects Go module download requests to repository:

* GOPRIVATE = list of paths that must bypass GOPROXY and GOSUMDB and download private modules directly from those VCS repos = значение по-умолчанию для низкоуровневых переменных GONOPROXY и GONOSUMDB, которые обеспечивают более точный контроль над тем, какие модули выбираются через прокси и проверяются с использованием базы данных контрольной суммы.

* GOSUMDB идентифицирует имя и, необязательно, открытый ключ и URL-адрес сервера базы данных для проверки контрольных сумм модулей, которые еще не перечислены в файле go.sum основного модуля.
