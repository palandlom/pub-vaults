#date #time #format


Вместо *yyyy-mm-dd* Го использует следующий шаблон для маркировки элементов даты-времени (часов, минут, дней) - вот его разные варианты

	Mon Jan 2 15:04:05 MST 2006
	01/02 03:04:05PM ‘06 -0700
	2006-01-02T15:04:05Z07:00

Таким образом, создавая свой шаблон надо использовать эти элементы - т.е для обознаения часов использовать - три часа *03*, месяца - январь, года - две тысячи шестой.

Готовые константы лаяутов:

	ANSIC       = "Mon Jan _2 15:04:05 2006"
	UnixDate    = "Mon Jan _2 15:04:05 MST 2006"
	RubyDate    = "Mon Jan 02 15:04:05 -0700 2006"
	RFC822      = "02 Jan 06 15:04 MST"
	RFC822Z     = "02 Jan 06 15:04 -0700"
	RFC850      = "Monday, 02-Jan-06 15:04:05 MST"
	RFC1123     = "Mon, 02 Jan 2006 15:04:05 MST"
	RFC1123Z    = "Mon, 02 Jan 2006 15:04:05 -0700"
	RFC3339     = "2006-01-02T15:04:05Z07:00"
	RFC3339Nano = "2006-01-02T15:04:05.999999999Z07:00"
	Kitchen     = "3:04PM"
	// Handy time stamps.
	Stamp      = "Jan _2 15:04:05"
	StampMilli = "Jan _2 15:04:05.000"
	StampMicro = "Jan _2 15:04:05.000000"
	StampNano  = "Jan _2 15:04:05.000000000"


### Example
#### дата и время с зоной
	layout := "2006-01-02T15:04:05Z07:00"  // time.RFC3339 - 
	value := "2015-09-15T14:00:12Z" // or 	"2015-09-15T14:00:12-00:00"

	if _, err := time.Parse(layout, value); err != nil || len(value) == 0 {
		fmt.Printf("Incorrect option: %q\n %s", value, err.Error())
	}
	
#### время 
	layout := "15:04:05"
	value := "14:00:12" 

	if _, err := time.Parse(layout, value); err != nil || len(value) == 0 {
		fmt.Printf("Incorrect option: %q\n %s", value, err.Error())
	}
	
#### дата
	layout := "2006.01.02"
	value := "1933.02.03" 

	if _, err := time.Parse(layout, value); err != nil || len(value) == 0 {
		fmt.Printf("Incorrect option: %q\n %s", value, err.Error())
	}