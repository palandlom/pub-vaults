# update ubuntu 18.04

Удаляем старый:
	
	sudo apt remove golang-go
	sudo apt autoremove
	
Качаем новый:

	cd ~
	wget https://golang.org/dl/go1.19.13.linux-amd64.tar.gz

Ставим:

	tar xvf ./go1.19.13.linux-amd64.tar.gz 
	sudo chown -R root:root ./go
	sudo rm -rf /usr/local/go
	sudo mv go /usr/local
	
Прописываем енв в 	`~/.profile`:
	
	export GOPATH=$HOME/work
	export PATH=$PATH:/usr/local/go/bin:$GOPATH/bin

... активируем енв:
	
	source ./.profile