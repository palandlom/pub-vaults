A package [[пакет]] = collection of source files in the same directory that are compiled together. 

(**папку с файлами пакета называть по имени пакета** - при импорте удобно).

Импорт +с  псевдонимом:
```bash
import (
 f "fmt"
  "math/rand"
)

func main() {
  for i := 0; i < 10; i++ {
    f.Printf("%d) %d\n", i, rand.Intn(25))
  }
}
```
