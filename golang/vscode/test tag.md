#test #flag #vscode 
Чтобы тесты с [[tag]] запускались из кнопки в vscode, нужно добавить в: 
* глобальный `~/.config/Code/User/settings.json` (см `ctr+shift+p` +  open settings (UI))
* проектный `.vscode/settings.json`
... `go.testFlags` c опциями и тэгами


Чтобы в тестах-с-тэгами [нормально работал](https://github.com/golang/tools/blob/master/gopls/doc/settings.md#buildflags-string) [[gopls]], нужно туда же добавить `"gopls":{"buildFlags":[]` :

```json
{
...

"go.testFlags": ["-race", "-v", "-tags=neo4j"],
"gopls": {
    "buildFlags": ["-tags=neo4j"]
  }
...

}
```
