#env #vars  #vscode 

## Назначение окр-переменных при запуске из vscode
`./.vscode/launch.json`:
```JSON
{
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        
        {
            "name": "Launch",
            "type": "go",
            "request": "launch", 
            "mode": "debug",            
            "program": "${workspaceFolder}",
            "envFile": "${workspaceFolder}/.env",
            "args": [], 
            "showLog": true,
            "buildFlags": "-tags nolicence"
          }
    ]
}
```

`./.env`:
```
AWS_ACCESS_KEY_ID=PNB32
AWS_SECRET_ACCESS_KEY=kqb4gRp
AWS_DEFAULT_REGION=us-west-1
```

## Назначение окр-переменных для тестов

.vscode/settings.json:
```json
{
"go.testEnvFile": "${workspaceFolder}/.env",
}
```
