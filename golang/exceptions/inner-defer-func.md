

Лучшее решение для [[recover]] - вложенная [[defer]]-функция, которая меняет [[именованный параметр]] = `panicError error`  от паникующей функции. 

Возвращаемое именнованое-значение `panicError error` указывает была ли паника в функции-паникере.

```
func (mng *parserManger) PanicFunction(str string) (panicError error) {
	panicError = nil
	
	// =============================================================
	// Defer-func to handle panic. 
	// It will set named-param panicError var - if panic will happen 
	// ==============================================================
	defer func() {
		if panicMessage := recover(); panicMessage != nil {
			// change name param - panicError
			panicError = errors.Newf("Panic error: %s", panicMessage)
		}
	}()

	// some code
	
	// !!! some panic !!!
	
	return // panicError in any case will be return
}
```


