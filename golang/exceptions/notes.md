Функция protect в приведенном ниже примере вызывает аргумент функции g и защищает вызывающих от паники во время выполнения, вызванной g.

```
func protect(g func()) {
    defer func() {
        // Println выполняется нормально, 
        // даже если есть паника
        log.Println("done")  
        if x := recover(); x != nil {
            log.Printf("run time panic: %v", x)
        }
    }()
    log.Println("start")
    g()
}
```

https://golang-blog.blogspot.com/2019/06/go-specification-panic-handling-recover.html