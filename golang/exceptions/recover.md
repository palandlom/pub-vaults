[[defer]] [[panic]] [[]]

Мы можем указать внутри функции `defer` сценарий восстановления:

```
func recoveryFunction()  {
  
  // ====================================================
  // === Check that PANIC happened + call recover() = 
  // === after finish we will return to Main
  // ====================================================
  if recoveryMessage := recover(); recoveryMessage != nil {
    fmt.Println(recoveryMessage)
  }
  
  fmt.Println("FINISH - recovery function...")
  // return to Main - FINISH
}



func executePanic() (int,bool) {

// === define recover function
  defer recoveryFunction()
  
  panic("!!!panic message!!!") //start defer-recoveryFunction()
  
  fmt.Println("UNREACHEABLE CODE")  
  return 1, true
}

func main() {
  int, ok := executePanic()
  fmt.Println("FINISH - Main ",int,ok)
}

```
В  `defer` мы вызываем функцию восстановления `recover`, которая возвращает `сообщение panic`, из функции `panic`. 
Так как в `defer`-функции вызвали `recover`, то после ее завершения вернемся в main.

Если есть паник-функция, ее надо вызывать через функцию-стартер, у которой есть дефер-рековери-функция.
- если паники не было, то функция-стартер вернет `результат паник-функции` и `True`-типа хорошо завершилась. 
- паника, то функция-стартер вернет `пустое-значение` и `False` как пустое логическое значение
- по `true`/`false` можно понять использовать ли полученное значение или нет