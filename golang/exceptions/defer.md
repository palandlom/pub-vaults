**defer** выполняются даже при появлении [[panic]]-ошибки.
 
`myDeferFunction` выполняется перед выходом из `main`:
 
```
func myDeferFunction() {
  fmt.Println("This is Simple Defer Function Execution")  
}

func main() {
  defer returnMessage()
  fmt.Println("This is line One")
  fmt.Println("This is line Two")
  fmt.Println("После этой строки вызывается returnMessage()")
}
```
