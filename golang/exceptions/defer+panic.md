Когда появляется [[panic]]-ошибка, отыскиваются внутри функции все ключевые слова [[defer]] и выполняются функции в них по-очереди.

```
func executePanic() {
  defer recoveryFunction()
  
  panic("This is Panic Situation")
  fmt.Println("The function executes Completely")  
}
```
