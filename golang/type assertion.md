---
aliases: [утверждение типа]
---
#type #assertion #casting

Statement asserts that the **interface value** i holds the **concrete type T** and assigns the underlying T value to the variable t. 

	t := i.(T)

### example
```go

	var i interface{} = "hello"

	s := i.(string)
	fmt.Println(s)

	s, ok := i.(string)
	fmt.Println(s, ok)

	f, ok := i.(float64)
	fmt.Println(f, ok)

	f = i.(float64) // panic
	fmt.Println(f)

```