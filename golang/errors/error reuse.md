#golang #errors 

Можно объявить ошибку заранее в переменной:
```golang
var ErrNoMatch = errors.New("no match error")
```
... затем возвращать ее. 
Также проверять эта ли ошибка вернулась, даже если оба обернута:
- обворачивание ошибки:
```golang
fmt.Errorf("dt [%s] already exist: %w", digitalTwinId, ekg.ErrEntityExists)
```
* проверка, что ошибка есть в цепочке ошибок т.е. ищет строковое значение ошибки:
```golang
err:= fmt.Errorf("dt [%s] already exist: %w", digitalTwinId, ekg.ErrEntityExists)
if errors.Is(err, ekg.ErrEntityExists) {...}
```
* проверка, что цепочке ошибок есть ошибка некоторого типа - т.е. структура, реализующая  интерфейс `Error`:
```golang
type BadInputError struct {
    input string
}

func (e *BadInputError) Error() string {
    return fmt.Sprintf("bad input: %s", e.input)
}

err:= fmt.Errorf("validateInput: %w", &BadInputError{input: input})


if errors.As(err, &e) {
    // err is a *QueryError, and e is set to the error's value
}
```


