 [[httpServer]]

 Для прямой отправки статических файлов в пакете `http` определена [[handler]]-функция `FileServer`, которая возвращает объект `Handler`:

	FileServer(root FileSystem) Handler
	
```golang
func main() {     
	// для корня сайта - fs-хендлер, который отдает файлы из корня.
    fs := http.FileServer(http.Dir("static"))
    http.Handle("/", fs)
     
    http.HandleFunc("/about", func(w http.ResponseWriter, r *http.Request){
        fmt.Fprint(w, "About Page")
    })
    http.HandleFunc("/contact", func(w http.ResponseWriter, r *http.Request){
        fmt.Fprint(w, "Contact Page")
    })
    fmt.Println("Server is listening...")
    http.ListenAndServe(":8181", nil)
}
```