#test #teardown #init


[TestMain](https://pkg.go.dev/testing#hdr-Main)
main_test.go:
```golang
package test

// === type = Cleanup and init Func
type InitFunc = func() error
type CleanupFunc = func() error

// === var = concreate cleanup func
var s3TestInitFunc func() CleanupFunc

var initFunctions map[string]InitFunc = make(map[string]InitFunc)
var cleanupFunctions map[string]CleanupFunc = make(map[string]CleanupFunc)

WTF? часть ф-й в карте, часть вынесена

func AddTestInitFunc(name string, f InitFunc) {      
	initFunctions[name] = f
}

func AddTestCleanupFunc(name string, f CleanupFunc) {
	cleanupFunctions[name] = f
}



func TestMain(m *testing.M) {

// === tearup code
	if s3TestInitFunc != nil {
		s3Cleanup = s3TestInitFunc()
	}

// === start tests
code := m.Run() 

// === teardown code
	if s3Cleanup != nil {
		if err := s3Cleanup(); err != nil {
			log.Err(err).Msgf("failed to cleanup s3 tests")
		}
	}


}
```



some_test.go:
```golang
package test

func init() {

	WTF? почему через переменные не объявлена

	cleanupFunc := func() error {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
		defer cancel()
		return PerformNEOWriteQuery(
			ctx,
			GetGeneralGOGMConn(),
			fmt.Sprintf(
				"MATCH (n) WHERE n.%s STARTS WITH '%s' DETACH DELETE n",
				ogm.DigitalTwinIdProperty,
				digitalTwinIDEntitiesNEOFunc,
			),
			nil,
		)
	}

	InitGeneralGogmConn()
	AddTestInitFunc(entitiesNEOFuncTestName, cleanupFunc)   
	AddTestCleanupFunc(entitiesNEOFuncTestName, cleanupFunc)
}

```