---
aliases: [запуск некоторых тестов]
---
#test #tag  #build #hint

# Запуск группы тестов
Переместить группу тестов файл. В файл добавить  [[tag]] и при тестировании отменять/ставить этот тэг.
```golang
// go:build integration
// +build integration

package main

import (
    "testing"
)

func TestThatRequiresNetworkAccess(t *testing.T) {
    t.Fatal("It failed!")
}
```

```bash
go test -tags integration
```
Команда собирает тест приложение, только из тест-файлов с тэгом `integration`
# Отключать тесты

```golang
// go:build !nointegration

package main

import (
    "testing"
)

func TestThatRequiresNetworkAccess(t *testing.T) {
    t.Fatal("It failed!")
}
```

```bash
go test -tags nointegration
```
