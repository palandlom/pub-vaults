[[httpServer]]

Подобная функция должна иметь два параметра: `func(w http.ResponseWriter, r *http.Request)`.

```func productsHandler(w http.ResponseWriter, r *http.Request) {
    vars := mux.Vars(r)
    id := vars["id"]
    response := fmt.Sprintf("Product %s", id)
    fmt.Fprint(w, response)
}
```

productsHandler - получает параметры пути запроса через функцию `mux.Vars`. Можно извлечь название нужного нам параметра: `id := vars["id"]`. 

Название параметра здесь то же самое, что в определение маршрута:
`router.HandleFunc("/products/{id:[0-9]+}", productsHandler)`

В итоге при обращении к приложению по запросу: 
`localhost:8181/products/2 `
... получим следующий вывод: 
`product 2`