
[httpserver]

#http #server #golang 

```package main
import (
    "fmt"
    "net/http"
    "github.com/gorilla/mux"
)
 
func productsHandler(w http.ResponseWriter, r *http.Request) {
    vars := mux.Vars(r)
    id := vars["id"]
    response := fmt.Sprintf("Product %s", id)
    fmt.Fprint(w, response)
}
 
func main() {
	// === 1 Задаем хендлер productsHandler
    
    // === 2 Создаем роутер - с роутом на хендлер  
    router := mux.NewRouter()
	// В фигурных - параметр в формате имя_параметра:регулярное_выражение.
	// id должен представлять числовое значение.
	// может быть просто имя-параметра e.g. {category}
    router.HandleFunc("/products/{id:[0-9]+}", productsHandler)

	// === 3 Помещаем роутер с хттп-сервер
    http.Handle("/",router)
    fmt.Println("Server is listening...")
    http.ListenAndServe(":8181", nil)
}
```