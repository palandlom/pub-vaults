#tag #hint #build 

Нужно разные константы/переменные использовать для сборки в разных окружениях. Для этого - выносим это-разное в отдельные файлы и в файлах ставим [[tag]]:

```golang
// !build dev

package config

const (
	MONGODB_HOST = "mongodb://127.0.0.1:27017"
)
```

```golang
// !build prod

package config

const (
	MONGODB_HOST = "mongodb://mongo.myserver.com:27017"
)
```

```bash
go build -tags dev 
go build -tags prod
```

