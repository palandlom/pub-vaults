Use separate file for `enum` - e.g: `./diff_lpmod_enum.go`:
```go
//go:generate stringer -type=LpModificationType ./diff_lpmod_enum.go

package cmd

type LpObjectModification struct {
}

type LpModificationType int

const (
	UpdateEntity LpModificationType = iota
	DeleteEntity
	CreateEntity
)

```
`iota` is an identifier that is used with `constant` and which can simplify constant definitions that use auto-increment numbers.

```bash
go get golang.org/x/tools/cmd/stringer
```
`go:generate stringer` generate `.String()` function.