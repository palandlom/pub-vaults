
`NumSender` шлет либо цифры, либо ошибку и выходит
`main` читает цифры ИЛИ если ошибка, то выходит

```golang 

func main() {
	valChan := make(chan int)
	errChan := make(chan error)
	defer close(errChan)
	defer close(valChan)

	go NumSender(valChan, errChan)

	for {
		select {
		// default:

		case er := <-errChan:
			fmt.Println(er.Error())
			return
		case v := <-valChan:
			fmt.Printf("val %d\n", v)
		}
	}
}


func NumSender(valChan chan<- int, errChan chan<- error) {

	randSource := rand.NewSource(time.Now().UnixNano())	
	rnd := rand.New(randSource)
	valLimit := 5

	for {
		val := rnd.Intn(1000)
		if val >= valLimit {
			valChan <- val
		} else {
			errChan <- errors.New(fmt.Sprintf("err %d", val))
			return
		}
	}
}

```