Гарантирует выполнение `f` только 1 раз при многократном вызове `once.Do(f)`
```go
var once sync.Once

once.Do(f func())
```