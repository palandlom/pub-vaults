[[goroutine]] [[deadlock]]

Мьютекс не блочит никакой переменной.
Это просто общий индикатор для нескольких рутин, того можно ли продолжать выполнение кода.

Т.е. когда одна из 5 рутин у себя в коде лочит мьютекс, то другие рутины в тех строках, где они хотят лочить этот же мьютекс - остановятся и будут ждать пока 1ая его не разлочит.

[ссылка](https://stackoverflow.com/questions/56719359/how-does-a-mutex-lock-know-which-variables-to-lock)

## Пример
```go
type Service struct {  
   mu   sync.RWMutex  
   Data string  
}  
  
func (s *Service) Read() {  
   s.mu.RLock()  
   defer s.mu.Unlock()  
   print(s.Data)  
}
```
