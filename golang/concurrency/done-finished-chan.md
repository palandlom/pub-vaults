#pattern
Основная функция запускает/останавливает дочернюю с помощью `done`-канала:
```go
done:= make(chan struct{})
defer close(done)

// Запуск дочерней c done
s.Run(done)
```

Дочерняя фунция "подписана" на `done`-канал и возвращает `finished`-канал.
В `finished`-канал она пошлет сигнал, когда завершиться
```go
func (s *Service) Run(done <-chan struct{}) <- chan struct{} {
	finshed:=make(chan struct{})
	...
	...
	return finished
}
```

В дочерней функции - выполнение работы до получения сигнала из `done`
```go
for {
	select {
		case <-done:
			return
		default:
			// work or ...
	}
	// ... work
}
```