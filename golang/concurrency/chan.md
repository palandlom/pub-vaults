#golang #concurrency #channel
[[bufchan]] [[goroutine]]

TODO
https://medium.com/rungo/anatomy-of-channels-in-go-concurrency-in-go-1ec336086adb
# channel
Рутины общаются через каналы. В рутинной-функции задается аргумент канал `mychan chan string` + когда функция вызывается в нее передеется канал:
```
var c chan string = make(chan string)
go pinger(c)
```
### Отправка в канал
  	c <- "ping"
### Чтение из канала 
	msg := <- c
... или просто вывод 
	
	fmt.Println(<-c)
	<- c
Если в канале ничего нет + он не буферизованный, то рутина на этой строке стопиться и ждет пока не появиться значение в канале:

### Ожидание сообщения из канала каждую секунду
``` golang
	for {
			msg := <- c
			fmt.Println(msg)
			time.Sleep(time.Second * 1)
		}
```
### Ожидание сообщения из канала и его вывод  + пока канал открыт
```golang
for {
			val, ok := <-c						
			if ok == false {	
				break // exit break loop
			} else {
				fmt.Println(val, ok)
			}
	}
```
... `range`-вариант:
``` golang
	c := make(chan int)
	go writeToChanelFunc(c) // start goroutine
	// periodic block/unblock of main goroutine until chanel closes
	for val := range c {
		fmt.Println(val)
	}
```
... т.е. функции (e.g. `go writeToChanelFunc(c)`), которые пишут канал должны его закрыть когда-нибудь иначе `for` не завершиться.

### Почитывание сообщений из канала в отдельной рутине

	go func(messageChan <-chan *messageChanel) {
		for mes := range messageChan {
			// mes logging
		}
	}(messageChan)

### Закрытие канала + проверка открыт ли он
Дизайн приложения должен быть таким, что сторона создающая канал, его же и закрывает и пишет в него то же она. Более того, в этих случая рекомендуется, что б функция возвращала канал только для чтения

  	c := make(chan int)
	close(c)	
	
	// check that chanell is not closed (open)
	// ... if so - write to it
	_, ok := <-c
	if ok != false {
		c <- "Mike"
	}

... закрытие канала не блочит текущую рутину.


### Функция только пишет в канал
	func pinger(c chan<- string)

### Функция только читает из канала
	func printer(c <-chan string)

### Канала шлет в себя время
```
ticker := time.NewTicker(duration)
```
... т.е. можно сделать рутину, которая будет что-то делать каждые `duration` секунд, если она будет слушать этот канал.

### select - слушанье многих каналов
`select` работает как `switch`, но для каналов.
Ожидание в отдельной рутине прихода сообщений (например, для их логирования):
```
go func() {
        for { //бесконечно "слушаем" 
            select {
            case msg1 := <- c1:
                fmt.Println(msg1)
            case msg2 := <- c2:
                fmt.Println(msg2)
			default:
				fmt.Println("nothing ready")
            }
        }
    }()
```

 Оператор `select` выбирает готовый канал ( `c1` или `c2`), и получает/передает сообщение через него. 
 Когда готовы несколько каналов - использование случайно выбранного готового канала. 
 Если ни один из каналов не готов и из-за того, что `select` идет внути `for`, то проверяется есть ли  `default:` блок :
 - есть - бесконечно выполняется код в нем
 - нет - блокирует ход программы/рутины до поступления сообщений.
 
### Чтение сообщений в течении N-секунд
```	
go func() {
		for {
			select {
			case msg1 := <-c1:
				fmt.Println(msg1)
			case msg2 := <-c2:
				fmt.Println(msg2)
			case <-time.After(time.Second):
				fmt.Println("timeout")
			}
		}
	}()
	
```
... `time.After(time.Second)` - пишет время в канал каждую секунду. Видимо в этом `case` можно проверять время и дропнуть рутиту через N секунд.

### Как завершить рутины по signterm-сигналу от системы
Создается канал `chan`, в который пишутся сигналы от ОС. 
В 3-м `case` эти сигналы проверяются - если пришли, то пишем в стоп-канал `struct{}{}`.
`стоп-канал` мониторится рутинами сервера и клиента - они завершаются по стоп сигналу.

```
// catch interrupt system signal for graceful shutdown of server and client
quit := make(chan os.Signal, 1)
signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)

LOOP:
		for {
			select {
			case err = <-clientErrChan:
				log.Logger.Warn().Err(err).Msg("received error from client side")
			case err = <-serverErrChan:
				log.Logger.Warn().Err(err).Msg("received error from server side")
			case <-quit:
				// stop running client and server
				log.Logger.Debug().Msg("received interrupt signal, closing reference monitor client and server")
				stopChan <- struct{}{}
				break LOOP
			}
		}
```
... мониторинг стоп-сигнала:
```
loop:
	for {
		select {
		case <-stopChan:
			errChan <- err // send error in case any error occurred
			break loop
		case <-ticker.C:
			log.Logger.Info().Msg("querying table processor")

			...
		}
	}
```

