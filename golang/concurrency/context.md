[[chan]] [[goroutine]]

#context #cancel #func

Контекст (кон) передается в функцию, чтобы она завершилась если "тупит".
Если эта функция вызывает другую, то она туда же отправляет этот контекст.

"Финальная" начинает выполнять работу и при этом мониторит done-канал контекста.
Если с этого канала, что-то пришло - нужно завершить заботу с ошибкой.

```go
func (c *Client) Run(ctx context.Context, req *Request, resp interface{}) error {
	// проверяем завершения контекста
	select {
	case <-ctx.Done():
		return ctx.Err()
	default:
	}
	
	// ... если нет, то делем что-то
	return c.runWithJSON(ctx, req, resp)
}
```

Запуск медленной-задачи в рутине + мониторинг завершения контектса:

```go
func sleepRandomContext(ctx context.Context, ch chan bool) {

    // Выполнение действий по очистке + (если есть) отмена созданных контекстов   defer func() {
        fmt.Println("sleepRandomContext complete")
        ch <- true
    }()

    // Создаем канал для результата от рутины с медленной-задачей
    sleeptimeChan := make(chan int)

    // Запускаем выполнение медленной задачи в горутине    
    go sleepRandom("sleepRandomContext", sleeptimeChan)

    // Используем select для выхода по истечении времени жизни контекста
    select {
        case <-ctx.Done():
            // Если контекст истекает, выбирается этот случай
            // Высвобождаем ресурсы
            // Посылаем сигнал всем горутинам, которые должны завершиться 
            
			// Обычно вы посылаете что-нибудь в канал 
            // или ждете выхода из горутины, затем возвращаетесь ??
            // или используете gw.Wait вместо каналов для синхронизации ??
            fmt.Println("Time to return")

        case sleeptime := <-sleeptimeChan:
            // выбирается, когда работа завершается до отмены контекста
            fmt.Println("Slept for ", sleeptime, "ms")
    }
}
```

## cancel func
Вызывается в той функции, в которой создана.
Обычно вызывается в `defer`
Вызывает `cancel` функции дочерних контекстов (видимо дочерние-контексты в своих `cancel`-функциях шлют мессадж в `<-ctx.Done()`)

```go
func main() {
 
 	// Создаем контекст background
    ctx := context.Background()
    // Производим контекст с функцией-отмены
    ctxWithCancel, cancelFunction := context.WithCancel(ctx)

    // Отложенная отмена высвобождает все ресурсы
    // для этого и производных от него контекстов
    defer func() {
       fmt.Println("Main Defer: canceling context")
       cancelFunction() // вызываем созданную функцию-отмены
    }()

	// рутина-для-примера - запускает функию-отмены через n-сек
    // Отмена контекста после случайного тайм-аута    
    go func() {
       sleepRandom("Main", nil)
       cancelFunction()
       fmt.Println("Main Sleep complete. canceling context")
    }()

    // Выполнение работы + отсылка созданного контекста
    doWorkContext(ctxWithCancel)
}
```

... функция, в которую отправлен указанный контекст:
```go
func doWorkContext(ctx context.Context) {

    // создает свой контекс от переданного со свое cancel-функцией
    ctxWithTimeout, cancelFunction := 
	context.WithTimeout(ctx, time.Duration(150)*time.Millisecond)

    // Функция отмены для освобождения ресурсов после завершения функции
    defer func() {
        fmt.Println("doWorkContext complete")
        cancelFunction()
    }()

    // Создаем канал и вызываем функцию контекста
    // Можно также использовать группы ожидания для этого конкретного случая,
    // поскольку мы не используем возвращаемое значение, отправленное в канал
    ch := make(chan bool)
    go sleepRandomContext(ctxWithTimeout, ch)

    // Используем select для выхода при истечении контекста
    select {
        case <-ctx.Done():
            // переданный в аргументе контекст уведомляет о завершении работы
            // это произойдёт, когда в main будет вызвана cancelFunction
            fmt.Println("doWorkContext: Time to return")

        case <-ch:
            // когда работа завершается до отмены контекста
            fmt.Println("sleepRandomContext returned")
    }
}
```

`.WithTimeout` возвращает производный контекст, который отменяется при вызове функции отмены или по истечении времени.

```go
ctx, cancel := context.WithTimeout(context.Background(), 2 * time.Second)
```