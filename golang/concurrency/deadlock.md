#golang #concurrency 
[[bufchan]] 

# Deadlock
Если в рутине А читать/писать в из/в канал, то эта рутина блокируется, а остальные (если они есть) - разблокируются. 		

Ожидается, что они запишут/считают, то что хочет прочитать / послала рутина А.

Если таких рутин нет, то - Deadlock
```

func main() {
	fmt.Println("main() started")

	c := make(chan string)
	c <- "John"
	
	fmt.Println("main() stopped")
}

fatal error: all goroutines are asleep - deadlock!
```
