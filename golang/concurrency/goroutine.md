#golang #concurrency #goroutine
[[bufchan]][[chan]]
# goroutine

Для создания горутины используется ключевое слово `go`, за которым следует вызов функции.

```
func f(n int) {
    for i := 0; i < 10; i++ {
        fmt.Println(n, ":", i)
    }
}

func main() {
    go f(0)
    var input string
    fmt.Scanln(&input)
}
```
... здесь присутствует вызов `Scanln`, без него программа завершится еще перед тем, как ей удастся вывести числа - т.к. основная рутина завешится.
