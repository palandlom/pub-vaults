`select` **случайным** образом выбирает канал **со значением** и читает из него. 
Т.е `select` как бы ожидает сразу несколько каналов = [[goroutine|рутина]] блокируется, ожидая с какого-нибудь значение из них.
```go
	select {	
	case er := <-errChan:
	case v := <-valChan:
	}
```
Обычно добавляют `case` c каким-нибудь "завершаюшим" каналом 
```go
	select {	
	case <- time.After(5 * time.Second): // пульнет 1 значение и закроется
		return
	case er := <-errChan:
	case v := <-valChan:
	}
	
	select {	
	case <-done: // завершающий канал
		return
	case er := <-errChan:
	case v := <-valChan:
	}

```
... или пустой `default`
```go
	select {	
	default:
	
	case er := <-errChan:
	case v := <-valChan:
	}
```

`for` зацикливает `select` = постоянно ожидаются значения с нескольких каналов.

```go
for {
	select {	
	case er := <-errChan:
	case v := <-valChan:
	}
}
```
