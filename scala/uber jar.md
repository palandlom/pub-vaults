[sbt-assembly repo](https://github.com/sbt/sbt-assembly)

Скачать плагин:
```bash
touch ./project/assembly.sbt
```
```sbt
addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "2.0.0")
```
Указать маин-класс:
```bash
touch ./project/build.sbt
```
```
mainClass := Some("HelloWorld")
```
... это маин-класс для:
```scala
@main def HelloWorld(args: String*): Unit =  
  println("Hello, World!")
```
Перезапустить сбт-консоль, чтобы плагин скачался - потом в ней:
```
assembly
```
