# Уставновка sbt
```
sudo apt-get update
sudo apt-get install apt-transport-https curl gnupg -yqq
echo "deb https://repo.scala-sbt.org/scalasbt/debian all main" | sudo tee /etc/apt/sources.list.d/sbt.list
echo "deb https://repo.scala-sbt.org/scalasbt/debian /" | sudo tee /etc/apt/sources.list.d/sbt_old.list
curl -sL "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x2EE0EA64E40A89B84B2DF73499E82A75642AC823" | sudo -H gpg --no-default-keyring --keyring gnupg-ring:/etc/apt/trusted.gpg.d/scalasbt-release.gpg --import
sudo chmod 644 /etc/apt/trusted.gpg.d/scalasbt-release.gpg
sudo apt-get update
sudo apt-get install sbt
```

# Создание проекта из шаблона
1. `cd` to an empty folder.
2. Run the following command `sbt new scala/scala3.g8`. This pulls the ‘scala3’ [template](https://www.scala-sbt.org/1.x/docs/sbt-new-and-Templates.html) from GitHub. 
3. When prompted, name the application `hello-world`. This will create a project called “hello-world”.
