# Структура
```scala
- root
    - project (sbt uses this to install and manage plugins and dependencies)
        - build.properties
    - src
        - main
            - scala (All of your scala code goes here)
                - Main.scala (Entry point of program)
    - build.sbt (sbt's build definition file)
```
В sbt основные параметры сборки хранятся в файле **build.sbt** в корневой директории проекта. Обратите внимание на пустые строки - они нужны:
```sbt
ThisBuild / version := "0.1.0-SNAPSHOT"  
  
ThisBuild / scalaVersion := "3.2.1"  
  
lazy val root = (project in file("."))  
  .settings(  
    name := "firstBlood"  
  )
```
Хранить версию sbt следует в `project/build.properties`

# Главный метод - точка входа
Должен быть метод `main` + `mainClass := Some("Main")`
```scala
object Main {
  def main(args: Array[String]): Unit =
    println("Hello, Scala developer!")
}
```
... или метод с аннотацией + `mainClass := Some("Runner")`
```scala
@main def Runner(args: String*): Unit = {  
  println("Hello, World!")  
}
```

