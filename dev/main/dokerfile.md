## composer

Запуск компосера:
```
docker-compose build
docker-compose up
```

Остановка и удаление контейнеров и другие ресурсы, созданные командой :
```
docker-compose up
docker-compose down
```

Выводит журналы сервисов:
```
docker-compose logs -f [service name]
```

Вывести список запущенный процесс:
```
docker-compose ps
```

Выполнить команду в выполняющемся контейнере:
```
docker-compose exec [service name] [command]
```

Вывести список образов:
```
docker-compose images
```

Перестроить и запустить только один сервис/контейнер:
```
docker-compose up -d --no-deps --build <service_name>
```
`--no-deps` - Don't start linked services.
`--build` - Build images before starting containers.

### composer yaml example
```
# Файл docker-compose должен начинаться с тега версии.
version: "3"

# Следует учитывать, что docker-composes работает с сервисами.
# 1 сервис = 1 контейнер.
# Раздел, в котором будут описаны сервисы, начинается с 'services'.
services:

  # Первый сервис (контейнер) c именем сервер.
  my-server:
 
    # Ключевое слово "build" = путь к Dockerfile сервиса
    # Здесь 'server/' соответствует пути к папке сервера,
    # которая содержит соответствующий Dockerfile.
    build: server/

    # Команда, которую нужно запустить внутри образа после его запуска
    command: python ./server.py

    
    # Если мы хотим обратиться к серверу с нашего компьютера (находясь за пределами контейнера),
    # мы должны организовать перенаправление этого порта на порт компьютера.
    # Сделать это нам поможет ключевое слово 'ports'.
    # При его использовании применяется следующая конструкция: 
    # 	[порт компьютера]:[порт контейнера]

    ports:
      - 1234:1234

  # Второй сервис (контейнер): клиент.
  my-client:
    build: client/
    command: python ./client.py

    # типа сети - указываем то, что контейнер может обращаться к 'localhost' 		 				   # компьютера.
    network_mode: host

    # Ключевое слово 'depends_on' позволяет указывать, должен ли сервис,
    # прежде чем запуститься, ждать, когда будут готовы к работе другие сервисы.
    # Нам нужно, чтобы сервис 'client' дождался бы готовности к работе сервиса 'server'.
    depends_on:
      - server
```


## docker dev

FROM - устанавливает базовый образ.

RUN - выполняет команду в контейнере:
... монтирует папку из build context в папку `/data` в контейнере:
```
RUN --mount=type=bind,target=/data команда
```
... можно монтировать некоторую папку из build context - `source=example`:
```
RUN --mount=type=bind,source=example,target=/data cp /data/* /root/
```

ENV - устанавливает переменную окружения:
```
ENV SITE_URL https://google.com/
```
... ее мона пользовать при запуске `-e SITE_URL=https://facebook.com/`:
```
docker run --rm -e SITE_URL=https://facebook.com/ test-curl
```

WORKDIR - устанавливает рабочую директорию:
```
WORKDIR /data 
```

Cоздает точку монтирования для тома - т.е. в контейнере будет эта папка:
```
VOLUME /data 
```
... ее можно будет замапить на папку при запуске контейнера:
```
docker run --rm -v $(pwd)/vol:/data/:rw test-curl
```

CMD — устанавливает исполняемый файл для контейнера.

```
RUN --mount=type=bind,target=/data cat /data/example/README.md > /root/README.md
```

### docker dev example
```
# родительский образ
FROM python:3-onbuild
MAINTAINER Prakhar Srivastav <prakhar@prakhar.me>

# install system-wide deps for python and node
# запуск команд в контейнере при создании
RUN apt-get -yqq update
RUN apt-get -yqq install python-pip python-dev
RUN apt-get -yqq install nodejs npm
RUN ln -s /usr/bin/nodejs /usr/bin/node

# добавление файлов с хоста в контейнер
ADD flask-app /opt/flask-app

# устанавливаем эту директорию в качестве рабочей, так что следующие 
# команды будут выполняться в контексте этой локации.
WORKDIR /opt/flask-app

# fetch app specific deps
RUN npm install

# !!! WORKDIR надо ставить перед ADD. Это ведь разные слои и в текущем порядке последний слой будет заново генерироваться при 
# каждом изменении файла проекта. Плюс прописывание команда только в композ файле, это не лучшее решение. Оставлять образ без точки входа нельзя. Любой образ должен уметь запускаться командой start без допов.

```

## doker commands
 Cмотрим список запущенных контейнеров:
`docker ps` `-a`/`-q` 

Cкачать образ:
`docker pull`

Cобирает образ:
... из указанного `build context` = текущей папки  + с меткой `test-curl`:
`docker build . -t test-curl`
 
Метка используется при запуске:
```
docker run test-curl
```

Логи контейнера:
`docker logs -f my_container_name`

Запускаем контейнер:
... с командой `/bin/echo 'Hello world'`
```
docker run ubuntu /bin/echo 'Hello world'
```
... c терминалом `-t` + с подосединением нему `-i` + с удалением контейнера `--rm`:
```
docker run -i -t –rm ubuntu /bin/bash
```
... с демонизацией контейнера под именем `--name daemon`:
```
docker run -d --name daemon ubuntu /bin/sh -c "while true; do echo hello world; sleep 1; done"
```
... с маппингом порта `-p 80:порт_контейнера` и текущей паки `-v $(pwd):папка_контейнера:режим` (!!! должны быть полные пути):
```
docker run -d --name test-nginx -p 80:80 -v $(pwd):/usr/share/nginx/html:ro nginx:latest
```
... запустить остановленный (без создания контейнера):
```
docker start my_container_name
```

Останавливает контейнер:
```
docker stop 
docker kill — «убивает» контейнер ...
docker rm — удаляет контейнер
docker rmi — удаляет образ
```
... останавливаем все запущенные контейнеры:
`docker kill $(docker ps -q)`

Удаляем все остановленные контейнеры: 
`docker rm $(docker ps -a -q)`

Удаляем все образы 
`docker rmi $(docker images -q)`

Cписок томов:
```docker volume ls```