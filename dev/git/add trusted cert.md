#problem #git #cert

	  fatal: unable to access 'https://pavel_lomov:oa3BvVw7Bp9hwsdioKe7@hakko.sekai.dev/sekai-backend/logic-api.git/': server certificate verification failed. CAfile: /etc/ssl/certs/ca-certificates.crt CRLfile: none
  
  The basic reason is that _your computer_ doesn't trust the _certificate authority_ that signed the certificate _used on the Gitlab server_.
  
  Check issuer:
   
   	echo -n | openssl s_client -showcerts -connect YOU_GITLAB_ADDR:443   2>/dev/null  | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p'   | openssl x509 -noout -text | grep "CA Issuers" | head -1
  
  Get entrusted cert:

	echo -n | openssl s_client -showcerts -connect YOU_GITLAB_ADDR:443   2>/dev/null  | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p'
	
Add it to your `</git_installation_folder>/bin/curl-ca-bundle.crt`:

	
